$(document).ready(function () {
  $(".dropdown-toggle").dropdown();
  $('li').addClass("list-group-item");
  const base_url = "https://gatesbooks-api.uw.r.appspot.com/";

  // weather update button click
  $("#getwx").on("click", function (e) {
    var mystation = $("input#book-year").val();
    var myurl = base_url + mystation;
    $("input#my-url").val(myurl);

    // clear out any previous data
    $("ul li").each(function () {
      // enter code to clear each li
      $("ul").empty();
    });

    console.log("Cleared Elements of UL");

    // execute AJAX call to get and render data
    $.ajax({
      url: myurl,
      dataType: "json",
      success: function (data) {
        console.log(data.books);
        
        for(var i=0; i < data.books.length; i++) {
          $("#bookrecs").append('<li class="list-group-item"> &quot;' + data.books[i].abstract + "&quot;" + 
            '<br><i>' +
              data.books[i].title +
              '"</i> by ' +
              data.books[i].author +
              '<br><img src="' +
              data.books[i].image +
              '" width="500" > </li>'
          );
        }

       
        var myJSON = JSON.stringify(data);
        $("textarea").val(myJSON);


        // add additional code here for the Wind direction, speed, weather contitions and icon image
      }
    });
  });
});